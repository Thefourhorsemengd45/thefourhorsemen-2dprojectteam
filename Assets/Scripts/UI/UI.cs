﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI : MonoBehaviour {
    //UI for player
    public Player player;
    public Sprite health4_4;
    public Sprite health3_4;
    public Sprite health2_4;
    public Sprite health1_4;
    public Sprite health0_4;
    public float maxHealth;
    public float minHealth;
    public float healthThreeQuator;
    public float healthHalf;
    public Image health;

    // Use this for initialization
    void Start () {
	
	}
	//Tracks the health based on players health settings.
	// Update is called once per frame
	void Update () {
        if(player.health >= maxHealth)
        {
            health.sprite = health4_4;
        }
        else if(player.health >= healthThreeQuator)
        {
            health.sprite = health3_4;
        }
        else if (player.health >= healthHalf)
        {
            health.sprite = health2_4;
        }
       else if (player.health >= minHealth)
        {
            health.sprite = health1_4;
        }
        else
        {
            health.sprite = health0_4;
        }

    }
}
