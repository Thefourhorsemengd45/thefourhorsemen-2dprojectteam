﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CooldownUI : MonoBehaviour {

    public GameObject player;
    private Animator anim;
    private WeaponRanged rangedweapon;
    public Image image;
    public Sprite[] sprites;

    private float cooldown;

	// Use this for initialization
	void Start () {
       
        anim = player.GetComponent<Animator>();
        rangedweapon = player.GetComponent<WeaponRanged>();
        cooldown = rangedweapon.cooldown;
       

    }
	//based on player cooldown time change the cooldown to specific image 
	// Update is called once per frame
	void Update () {
        if (rangedweapon.attackTimer <= 0)
        {
            image.sprite = sprites[0];
        }
     
        else if (rangedweapon.attackTimer <= (cooldown / 10) * 1)
        {
            image.sprite = sprites[9];
        }
        else if (rangedweapon.attackTimer <= (cooldown / 10) * 2)
        {
            image.sprite = sprites[8];
        }
        else if (rangedweapon.attackTimer <= (cooldown / 10) * 3)
        {
            image.sprite = sprites[7];
        }
        else if (rangedweapon.attackTimer <= (cooldown / 10) * 4)
        {
            image.sprite = sprites[6];
        }
        else if (rangedweapon.attackTimer <= (cooldown / 10) * 5)
        {
            image.sprite = sprites[5];
        }
        else if (rangedweapon.attackTimer <= (cooldown / 10) * 6)
        {
            image.sprite = sprites[4];
        }
        else if (rangedweapon.attackTimer <= (cooldown / 10) * 7)
        {
            image.sprite = sprites[3];
        }
        else if (rangedweapon.attackTimer <= (cooldown / 10) * 8)
        {
            image.sprite = sprites[2];
        }
        else if (rangedweapon.attackTimer <= (cooldown / 10) * 9)
        {
            image.sprite = sprites[1];
        }
        else
        {
            image.sprite = sprites[10];
        }

    }
}
