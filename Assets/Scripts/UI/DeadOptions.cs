﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class DeadOptions : MonoBehaviour
{


    [HideInInspector]
    public static bool GamePaused = false;
    public EventSystem ES;
    private GameObject StoreSelected;
    public Player player;
    public GameObject firstselected;
    public GameObject canvas;
    private bool started = false;
    public float deathScreenWait = 1;
    private Scene currentscene;

    public AudioClip buttonClip;

    // Use this for initialization
    void Start()
    {
        StoreSelected = firstselected;
        Time.timeScale = 1;

        hidePaused();
    }

    // Update is called once per frame
    void Update()
    {
        
        //checks players health of 0 stop game and show death screen
        if (player.health <= 0)
        {
            if (!started)
            {
                StartCoroutine(showPaused());
            }
        }
        if (ES.currentSelectedGameObject != StoreSelected)
        {
            if (ES.currentSelectedGameObject == null)
            {
                ES.SetSelectedGameObject(StoreSelected);
            }
            else
            {
                StoreSelected = ES.currentSelectedGameObject;
                GetComponent<AudioSource>().PlayOneShot(buttonClip);
            }
        }
    }


    //Reloads the Level
    public void Reload()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    //controls the pausing of the scene
    public void pauseControl()
    {
        if (GamePaused == false)
        {
            GamePaused = true;
            Time.timeScale = 0;
            showPaused();

        }
        else
        {
            GamePaused = false;
            Time.timeScale = 1;
            hidePaused();
        }
    }

    //shows objects with ShowOnPause tag
   IEnumerator showPaused()
    {
        started = true;
        yield return new WaitForSeconds(deathScreenWait);
        canvas.SetActive(true);
        //bug fix if mouse is presed
        ES.SetSelectedGameObject(firstselected);
        yield break;

    }

    //hides objects with ShowOnPause tag
    protected void hidePaused()
    {
        canvas.SetActive(false);
    }

    //loads inputted Scene
    public void LoadScene(int SceneToChangeTo)
    {
        SceneManager.LoadScene(SceneToChangeTo);
    }

    //exit game
    public void Quit()
    {
        Application.Quit();
    }


}
