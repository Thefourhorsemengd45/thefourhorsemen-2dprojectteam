﻿
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuOptions : MonoBehaviour
{
    GameObject[] mainMenu;
    GameObject[] pauseObjects;
    GameObject[] controlObjects;
    GameObject[] creditsObjects;
    GameObject[] optionsPanel;
    GameObject[] levelpanel;
    GameObject optionsTint;
    [HideInInspector]
    public static bool GamePaused = false;
    public EventSystem ES;
    private GameObject StoreSelected;
    private GameObject previusSelected;
    public GameObject level;
    public GameObject credites;
    public GameObject options;
    public AudioClip buttonClip;
    private bool menuup = false;


    // Use this for initialization
    void Start()
    {
        //checks if certain items exist in the sceen and set them acordingly
        StoreSelected = ES.firstSelectedGameObject;
        Time.timeScale = 1;
        if(GameObject.FindGameObjectsWithTag("ShowMainMenu" ) != null)
        {
            mainMenu = GameObject.FindGameObjectsWithTag("ShowMainMenu");
        }
        if(GameObject.FindGameObjectsWithTag("ShowOnPause") != null)
        { pauseObjects = GameObject.FindGameObjectsWithTag("ShowOnPause"); }
        if (GameObject.FindGameObjectsWithTag("ShowOnControls")!= null)
        { controlObjects = GameObject.FindGameObjectsWithTag("ShowOnControls"); }
        if (GameObject.FindGameObjectsWithTag("ShowOnCredits") != null)
        { creditsObjects = GameObject.FindGameObjectsWithTag("ShowOnCredits"); }
        if (GameObject.FindGameObjectsWithTag("ShowOptionsMenu") != null)
        { optionsPanel = GameObject.FindGameObjectsWithTag("ShowOptionsMenu"); }
        if (GameObject.FindGameObjectsWithTag("ShowOnLevels") != null)
        { levelpanel = GameObject.FindGameObjectsWithTag("ShowOnLevels"); }
        if (GameObject.FindGameObjectWithTag("ShowOptiosMenutint") != null)
        {
            optionsTint = GameObject.FindGameObjectWithTag("ShowOptiosMenutint");
        }
        hidePaused();
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        //bug fix if mouse is presed
        if(ES.currentSelectedGameObject != StoreSelected)
        {
            if(ES.currentSelectedGameObject == null)
            {
                ES.SetSelectedGameObject(StoreSelected);
            }
            else
            {
                StoreSelected = ES.currentSelectedGameObject;
                GetComponent<AudioSource>().PlayOneShot(buttonClip);
            }
        }
        //uses the p button to pause and unpause the game
        if (Input.GetButtonDown("Pause"))
        {
            if(pauseObjects.Length > 0)
            pauseControl();
            GetComponent<AudioSource>().PlayOneShot(buttonClip);
        }
        if(Input.GetButtonDown("Back"))
        {
            if (menuup == true)
            {
                hideControls();
                hideCredits();
                HideOptionsPanel();
                HideLevel();
            }
        }
    }


    //Reloads the Level
    public void Reload()
    {
        SceneManager.LoadScene(1);
    }

    //controls the pausing of the scene
    public void pauseControl()
    {
        if (GamePaused == false)
        {
            GamePaused = true;
            Time.timeScale = 0;
            showPaused();
        }
        else
        {
            GamePaused = false;
            Time.timeScale = 1;
            hidePaused();
        }
    }

    //shows objects with ShowOnPause tag
    protected void showPaused()
    {
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(true);
        }
    }

    //hides objects with ShowOnPause tag
    protected void hidePaused()
    {
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(false);
        }
        foreach (GameObject o in optionsPanel)
        {
            o.SetActive(false);
        }
        foreach(GameObject c in controlObjects)
        {
            c.SetActive(false);
        }
        foreach (GameObject cred in creditsObjects)
        {
            cred.SetActive(false);
        }
        foreach (GameObject l in levelpanel)
        {
            l.SetActive(false);
        }
        if (optionsTint != null)
        optionsTint.SetActive(false);

    }

    //loads inputted Scene
    public void LoadScene(int SceneToChangeTo)
    {
        SceneSetter.SetScene(SceneToChangeTo);
        SceneManager.LoadScene(1);
    }

    //shows controls when requested
    public void ShowControls()
    {
        menuup = true;
        previusSelected = ES.currentSelectedGameObject;
        foreach (GameObject c in controlObjects)
        {
            c.SetActive(true);
        }
        HideMenu();
        GetComponent<AudioSource>().PlayOneShot(buttonClip);

    }
    //hides controls when requested
    protected void hideControls()
    {
        menuup = false;
        foreach (GameObject c in controlObjects)
        {
            c.SetActive(false);
        }
        ShowMenu();
        StoreSelected = previusSelected;
        ES.SetSelectedGameObject(previusSelected);
        GetComponent<AudioSource>().PlayOneShot(buttonClip);
    }

    //exit game
    public void Quit()
    {
        Application.Quit();
    }

    //Credits
    public void ShowCredits()
    {
        menuup = true;
        previusSelected = ES.currentSelectedGameObject;
        foreach (GameObject cred in creditsObjects)
        {
            cred.SetActive(true);
        }
        HideMenu();
        StoreSelected = credites;
        ES.SetSelectedGameObject(credites);
        GetComponent<AudioSource>().PlayOneShot(buttonClip);

    }
    public void hideCredits()
    {
        menuup = false;
        foreach (GameObject cred in creditsObjects)
        {
            cred.SetActive(false);
        }
        ShowMenu();
        StoreSelected = previusSelected;
        ES.SetSelectedGameObject(previusSelected);
        GetComponent<AudioSource>().PlayOneShot(buttonClip);
    }
    //Call this function to activate and display the Options panel during the main menu
    public void ShowOptionsPanel()
    {
        menuup = true;
        previusSelected = ES.currentSelectedGameObject;
        foreach (GameObject o in optionsPanel)
        {
            o.SetActive(true);
        }
        optionsTint.SetActive(true);
        HideMenu();
        StoreSelected = options;
        ES.SetSelectedGameObject(options);
        GetComponent<AudioSource>().PlayOneShot(buttonClip);
    }

    //Call this function to deactivate and hide the Options panel during the main menu
    public void HideOptionsPanel()
    {
        menuup = false;
        foreach (GameObject o in optionsPanel)
        {
            o.SetActive(false);
        }
        optionsTint.SetActive(false);
        ShowMenu();
        StoreSelected = previusSelected;
        ES.SetSelectedGameObject(previusSelected);
        GetComponent<AudioSource>().PlayOneShot(buttonClip);
    }

    public void ShowLevels()
    {
        menuup = true;
        previusSelected = ES.currentSelectedGameObject;
        foreach (GameObject l in levelpanel)
        {
            l.SetActive(true);
        }
        optionsTint.SetActive(true);
        HideMenu();
        StoreSelected = level;
        ES.SetSelectedGameObject(level);
        GetComponent<AudioSource>().PlayOneShot(buttonClip);
    }
    public void HideLevel()
    {
        menuup = false;
        foreach (GameObject l in levelpanel)
        {
            l.SetActive(false);
        }
        optionsTint.SetActive(false);
        ShowMenu();
        ES.SetSelectedGameObject(previusSelected);
        GetComponent<AudioSource>().PlayOneShot(buttonClip);
    }
    //shows menu when requested
    void ShowMenu()
    {
       
        foreach (GameObject c in mainMenu)
        {
            c.SetActive(true);
        }


    }
    //hides menu when requested
     void HideMenu()
    {
        foreach (GameObject c in mainMenu)
        {
            c.SetActive(false);
        }
    }
}
