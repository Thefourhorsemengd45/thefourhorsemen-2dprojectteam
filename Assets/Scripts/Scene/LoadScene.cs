﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour {
    public int setScene = 0;

    //Load next level when player reaches the end
    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.GetComponent<Player>() != false)
        {
            SceneSetter.SetScene(setScene);
            SceneManager.LoadScene(1);
            
        }
    }
}
