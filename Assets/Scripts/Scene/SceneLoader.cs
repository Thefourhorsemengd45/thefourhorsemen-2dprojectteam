﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

    private bool loadScene = false;

    [SerializeField]
    private int scene;
    [SerializeField]
    private Image loading;
    private bool load = true;
    public Sprite[] loadingScene;
     private int i = 0;

    // Updates once per frame
    void Update()
    {
        if (load == true)
        {
            loadScene = true;
            load = false;
          // start a coroutine that will load the desired scene.
            StartCoroutine(LoadNewScene());

        }
        if(loadScene == true)
        {
            loadScene = false;
            StartCoroutine(ScrenSwitch());
        }
           

         }

         //Start an asynchronous operation to load the scene that was passed to the LoadNewScene coroutine.
         IEnumerator LoadNewScene()
         {

            scene = SceneSetter.GetScene();

             AsyncOperation async = SceneManager.LoadSceneAsync(scene);


             while (!async.isDone)
             {
                 yield return null;
             }

    }
    //switch screen
    IEnumerator ScrenSwitch()
    {
        loadScene = false;
        loading.sprite = loadingScene[i]; 
       yield return new WaitForSeconds(.2f);
        i++;
        if(i >= loadingScene.Length)
        {
            i = 0;
        }
        loadScene = true;
        yield return null;
    }
}