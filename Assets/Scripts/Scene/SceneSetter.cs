﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public  class SceneSetter : MonoBehaviour {
    private static int nextScene;

	// Use this for initialization
	//set the scene and gets sceen

    public static void SetScene(int next)
    {
        nextScene = next;
    }

    public static int GetScene()
    {
        return nextScene;
    }

}
