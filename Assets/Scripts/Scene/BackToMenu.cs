﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class BackToMenu : FadeInAndOut {

    public float secondsForCredits = 7;

	// Use this for initialization
	void Start () {
        StartCoroutine(StartNow());
        StartCoroutine(WinMenu());
    }
	//animation for credits when wining
IEnumerator WinMenu()
    {
        yield return new WaitForSeconds(secondsForCredits);
        if (fadeOutOnStart == false)
        {          
            FadeOut();
        }
        else
        {
            FadeIn();
        }
        yield return null;
    }
}
