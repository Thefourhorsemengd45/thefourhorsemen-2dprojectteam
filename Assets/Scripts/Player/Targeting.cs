﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Targeting : MonoBehaviour
{
    //list of targets 
    public List<Transform> targets;
    //selected target for attack
    private Transform selectedTarget;
    //targeting distance
    public float targetDistance = 10f;
    //target object
    public GameObject target;
    Animator anim;

    // Use this for initialization
    void Start()
    {
        //sets targets to the list
        targets = new List<Transform>();
        //set animator to units animator
        anim = GetComponent<Animator>();
        //add all enemies to targets
        AddAllEnemies();
        //select closest target
        TargetEnemy();
    }

    //adds enimies to list with tag of hostile
    public void AddAllEnemies()
    {
        //gets all units with the tage of Hostile
        GameObject[] hostiles = GameObject.FindGameObjectsWithTag("Hostile");
        targets = new List<Transform>();
        List<Transform> inRange = new List<Transform>();
        //loops through all hostiles in game object
        foreach (GameObject hostile in hostiles)
        {
            //if the hostile is within target distance
            if (Vector2.Distance(hostile.transform.position, transform.position) < targetDistance)
            {
                if (RangedHostileAlive(hostile) || MeleeHostileAlive(hostile) || isSwithch(hostile))
                {
                    inRange.Add(target.transform);
                    //check direction Player is faceing and if there are hostiles in that direction
                    Vector2 dir = (hostile.transform.position - transform.position).normalized;
                    float direction = Vector2.Dot(dir, new Vector2(anim.GetFloat("HorizontalSpeed"), anim.GetFloat("VerticalSpeed")));
                    //if there is add the hostile to the list
                    if (direction > 0)
                    {

                        AddTarget(hostile.transform);
                    }
                }

            }

        }
        if(inRange.Count > 0)
        {
            AmbientSounds.isInCombat = true;
        }
        else
        {
            AmbientSounds.isInCombat = false;
        }

    }

    //sort all targets in range by distance to the player
    private void SortTargetsByDistance()
    {
        //sorts targtes in list by their distance to the player
        targets.Sort(delegate (Transform t1, Transform t2)
        {
            return Vector2.Distance(
                t1.position, gameObject.transform.position).CompareTo(
                Vector2.Distance(t2.position, gameObject.transform.position));
        });
    }

    //adds target if within distance
    protected void AddTarget(Transform hostile)
    {
        if (Vector2.Distance(hostile.position, gameObject.transform.position) <= targetDistance)
        {
            targets.Add(hostile);
        }
    }

    //selects closest hostile as the target
    private void TargetEnemy()
    {
        //if there is targets deselect curent one 
        //then sort target list and select the first one
        if (targets.Count != 0)
        {
            DeselectTarget();
            SortTargetsByDistance();
            selectedTarget = targets[0];
            SelectTarget();
        }

        //else deselect target as there are none left
        else
        {
            DeselectTarget();
            target = gameObject;
        }



    }

    //selected target is changed color to show they are the target (will not be in final build bu exist for testing purposes)
    private void SelectTarget()
    {

        //selectedTarget.GetComponent<SpriteRenderer>().color = Color.blue;

        target = selectedTarget.gameObject;
    }
    //deselects target that was selected
    private void DeselectTarget()
    {
        if (selectedTarget != null)
        {
            //selectedTarget.GetComponent<SpriteRenderer>().color = Color.red;
            selectedTarget = null;
        }
    }

    //checks for enemies and targets in range
    // Update is called once per frame
    void Update()
    {
        AddAllEnemies();
        TargetEnemy();
    }

    private bool MeleeHostileAlive (GameObject hostile)
    {
        if (hostile.GetComponent<HostileMelee>() != null)
        {
            if (hostile.GetComponent<HostileMelee>().health > 0)
            {
                return true;
            }
            else return false;
        }
        else return false;
    }
    private bool RangedHostileAlive(GameObject hostile)
    {
        if (hostile.GetComponent<HostileRanged>() != null)
        {
            if (hostile.GetComponent<HostileRanged>().health > 0)
            {
                return true;
            }
            else return false;
        }
        else return false;
    }

    private bool isSwithch(GameObject hostile)
    {
        if (hostile.GetComponent<BaseHostile>() != null)
        {
            if (hostile.GetComponent<BaseHostile>().health > 0)
            {
                return true;
            }
            else return false;
        }
        else return false;
    }
}
