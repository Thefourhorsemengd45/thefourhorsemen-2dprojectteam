﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : BaseUnit
{
    public float lowHealth = 2;

    private bool isAttacking = false;

    public PlayerSound sound;

    // Update is called once per frame
    void Update()
    {
        if (MenuOptions.GamePaused == false)
        {
            if (health > 0)
            {
                if(health < lowHealth)
                {
                    sound.TiredAudio();
                }
                else
                {
                    sound.StopClip();
                }
                //Recives player input of left right up and down
                float horizontalInput = Input.GetAxis("Horizontal") * speed;
                float verticalInput = Input.GetAxis("Vertical") * speed;
                if (isAttacking == false)
                {
                    attacking = false;
                    //moves the player basied on input (Method located in BaseUnit)
                    Move(horizontalInput, verticalInput);

                    //if the player presses the x button or mouse 0
                    if (Input.GetButtonDown("Fire1"))
                    {
                        if (melee.attackTimer <= 0)
                        {
                            isAttacking = true;
                            anim.SetBool("Attack", isAttacking);
                        }
                    }

                    //if the player presses the b button or mouse 1
                    if (Input.GetButtonDown("Fire2"))
                    {
                        if (ranged.attackTimer <= 0)
                        {
                            isAttacking = true;
                            anim.SetBool("RangedAttack", isAttacking);
                        }
                    }
                }
                else if(isAttacking == true)
                {
                    attacking = true;
                    Move(0, 0);
                }
            }
            else
            {
                Idle();
                anim.SetBool("Death", true);
            }

        }
        else
        {
        }
    }
    //melee attack start chain
    public void DoMeleeHit()
    {
        if(!coroutineIsRunning)
        StartCoroutine(MeleeAttack());
    }
    //ranged attack start chain
    public void DoRangedHit()
    {
        if(!coroutineIsRunning)
       StartCoroutine(RanedAttack());
    }

    IEnumerator MeleeAttack()
    {
        coroutineIsRunning = true;
        isAttacking = true;

        float dir = melee.HostileDirection();

        yield return StartCoroutine(DoMeleeAttack(dir));

        yield return null;
    }

    IEnumerator RanedAttack()
    {
        coroutineIsRunning = true;
        isAttacking = true;

        yield return StartCoroutine(DoRangedAttack());
        
        yield return null;
    }
    
    IEnumerator DoRangedAttack()
    {
        ranged.FireProjectile();
        yield return new WaitForSeconds(animationTime);
        isAttacking = false;
        coroutineIsRunning = false;
        yield break;
    }

    IEnumerator DoMeleeAttack(float dir)
    {
        melee.MeleeAttack(dir);
       yield return new WaitForSeconds(animationTime);
        isAttacking = false;
        coroutineIsRunning = false;
        yield break;
    }
}
