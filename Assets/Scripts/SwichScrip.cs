﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwichScrip : BaseSounds {
    public BaseUnit self;
    private Animator anim;
    public AudioClip[] switchFlipped;
    // Use this for initialization
    void Start () {
        self = GetComponent<BaseUnit>();
        anim = GetComponent<Animator>();
        source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        //when the swtich is hit it becomes "dead" and plays the switch animation
        if(self.health <= 0)
        {
            if (anim.GetBool("IsDead") == false)
            {
                StartCoroutine(TurnSwitch());
            }
        }
		
	}
    //turns the switch activates the anmaption
    IEnumerator TurnSwitch()
    {
        anim.SetBool("IsDead", true);
        SwitchAudio();
        yield break;
    }

    //plays the audio
    public void SwitchAudio()
    {
        AudioClip audio = switchFlipped[Random.Range(0, switchFlipped.Length)];
        SetAudio(audio);
    }
}
