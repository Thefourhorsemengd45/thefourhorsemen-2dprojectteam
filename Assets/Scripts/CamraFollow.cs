﻿using UnityEngine;
using System.Collections;

public class CamraFollow : MonoBehaviour {
    //camra following the player
    public Transform target;
    //the speed at which the camera moves
    public float damping = 1;
    //distance which the camera can look ahead of the player
    public float lookAheadFactor = 3;
    //speed the camera will return when ahead
    public float lookAheadReturnSpeed = 0.5f;
    //speed at which the camera will dift ahead
    public float lookAheadMoveThreshold = 0.1f;

    private float m_OffsetZ;
    private Vector3 m_LastTargetPosition;
    private Vector3 m_CurrentVelocity;
    private Vector3 m_LookAheadPos;

    // Use this for initialization
    private void Start()
    {
        //sets camera to player position
        m_LastTargetPosition = target.position;
        m_OffsetZ = (transform.position - target.position).z;
        transform.parent = null;
    }


    // Update is called once per frame
    private void Update()
    {
        // only update lookahead pos if accelerating or changed direction
        float xMoveDelta = (target.position - m_LastTargetPosition).x;
        bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;

        if (updateLookAheadTarget)
        {
            m_LookAheadPos = lookAheadFactor * Vector3.right * Mathf.Sign(xMoveDelta);
        }
        else
        {
            m_LookAheadPos = Vector3.MoveTowards(m_LookAheadPos, Vector3.zero, Time.deltaTime * lookAheadReturnSpeed);
        }

        //changes position bay geting the player movement and if it is dampened or not
        Vector3 aheadTargetPos = target.position + m_LookAheadPos + Vector3.forward * m_OffsetZ;
        Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref m_CurrentVelocity, damping);

        //sets the camera position to the player dependeing on dampening
        transform.position = newPos;

        m_LastTargetPosition = target.position;
    }
}