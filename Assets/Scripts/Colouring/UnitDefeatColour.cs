﻿using UnityEngine;
using System.Collections;

public class UnitDefeatColour: BaseColour {

    // will be used for when a unit is defeated colour is added back to the world

    public float minLight = 0.0001f;
    public float maxLight = 20f;
    public float fadeTime = 1;
    private float tParam = 0;
    private float totalNodeHealth;
    private bool hasStarted = false;
    public string colourType;
    public float secondstowaitperframe;

    public BaseUnit[] Hostiles;


    void Start()
    {
        //sets the nodes location on the greyplane
        foreach(GameObject greyplane in greyplanes)
        {
            Vector3 origin = new Vector3(0, offsetY, 0);
            origin += target.position;
           // if (GetComponent<Renderer>() != null)
           // {
                greyplane.GetComponent<Renderer>().material.SetVector(colourSource, origin);
          //  }
           // else
           // {
             //   greyplane.GetComponentInChildren<Renderer>().material.SetVector(colourSource, origin);
          //  }
        }
        
    }

    // Update is called once per frame
    void Update()
    {

        
        if (tParam <= 1)
        {
            totalNodeHealth = 0;
            //checks all connected hostiles
            foreach (BaseUnit hostile in Hostiles)
            {
                totalNodeHealth += hostile.health;
            }

            if (totalNodeHealth == 0)
            {

                if (!hasStarted)
                {
                    hasStarted = true;
                    StartCoroutine(allUnitsDefeated());
                }
            }
            
        }


    }
    //when all units conected to the node are killed expand the colour
    IEnumerator allUnitsDefeated()
    {
        
            tParam += Time.deltaTime / fadeTime;
            var lerpedValue = Mathf.Lerp(maxLight, minLight, tParam);
        foreach (GameObject greyplane in greyplanes)
        {
                greyplane.GetComponent<Renderer>().material.SetFloat(colourType, lerpedValue);
            
        }

        yield return new WaitForSeconds(secondstowaitperframe);
        hasStarted = false;
        yield return null;
        
    }
}
