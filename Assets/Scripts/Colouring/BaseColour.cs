﻿using UnityEngine;
using System.Collections;


public class BaseColour : MonoBehaviour {
    //offset for the Colour location
    public float offsetY = -.04f;
    //objecct that is greyscaled
    public GameObject[] greyplanes;
    //what colour additor the object uses from the shader
    public string colourSource;
    //target of the colour location
    public Transform target;


    //Greyscale offset positiom and seting the size and location of the specific colour 
    //location in the Shader material.
    public void GreyScale(float offsetY, float maxradius, float radius)
    {
        Vector3 origin = new Vector3(0, offsetY, 0);
        origin += target.position;

        foreach (GameObject greyplane in greyplanes)
        {
            greyplane.GetComponent<Renderer>().material.SetVector(colourSource, origin);
            greyplane.GetComponent<Renderer>().material.SetFloat("_PColourMaxRadius", maxradius);
            greyplane.GetComponent<Renderer>().material.SetFloat("_PColourRadius", radius);
        }
    }
}
