﻿using UnityEngine;
using System.Collections;

public class CharaterColour : BaseColour
{

    private Vector2 velocity = Vector2.zero;
    //colour radius size around the player
    public float colourRadius = 1.7f;
    //maximum colour radius for the players colouring
    public float maxColourRadius = 0.01f;




    // Update is called once per frame
    void Update()
    {
        //colour location around player (located in BaseColour)
        GreyScale(offsetY, maxColourRadius, colourRadius);

    }

}