﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : BaseSounds {
    //The amount of health this pickup gives the player.
    public float healthIncrease = 1;
    public AudioClip[] pickupAcquired;

    // Use this for initialization
    void Start()
    {
        source = GetComponent<AudioSource>();
    }
    // When the player collides with the pickup...
    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.CompareTag("Player"))
        {
            if (other.GetComponent<Player>() != null)
            {
                //Increase the player's health by healthIncrease and destroy the pickup.
                other.GetComponent<Player>().health += healthIncrease;
                //plays the pick up audio after the player eats the fruit.
                //Disable the visuals and collider after the player collides with it.
                this.GetComponent<SpriteRenderer>().enabled = false;
                this.GetComponent<BoxCollider2D>().enabled = false;
                StartCoroutine(PickupAudio());
            }
        }
    }

   IEnumerator PickupAudio()
    {
        AudioClip audio = pickupAcquired[Random.Range(0, pickupAcquired.Length)];
        SetAudio(audio);
        yield return new WaitForSeconds(2);

        this.gameObject.SetActive(false);
        yield return null;
    }
}
