﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostileMelleSounds : BaseSounds
{
    //Audio for melee hostiles
    //set up in ispector
    public AudioClip meleeEnemy_Death;
    public AudioClip[] meleeEnemy_Attacks;
    public AudioClip[] enemyHit;
    public AudioClip[] meleeEnemy_Walks;



    // Update is called once per frame


    public void MeleeAttackAudio()
    {
        AudioClip audio = meleeEnemy_Attacks[Random.Range(0, meleeEnemy_Attacks.Length)];
        SetAudio(audio);
    }
    public void HitAudio()
    {
        AudioClip audio = enemyHit[Random.Range(0, enemyHit.Length)];
        SetAudio(audio);
    }
    public void meleeWalksAudio()
    {
        AudioClip audio = meleeEnemy_Walks[Random.Range(0, meleeEnemy_Walks.Length) ];
        SetAudio(audio);
    }

    public void MeleeEnemy_DeathAudio()
    {
        source.Stop();
        source.PlayOneShot(meleeEnemy_Death);
    }


}
