﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseSounds : MonoBehaviour {
    //genaric audio setter for units
    protected AudioSource source;

    void Start()
    {
        source = GetComponent<AudioSource>();
    }
    protected void SetAudio(AudioClip audio)
    {
        source.PlayOneShot(audio);
    }
}
