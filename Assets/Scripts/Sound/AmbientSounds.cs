﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientSounds : MonoBehaviour
{

    //all sounds set in inspector
    //  public AudioClip lowIntensity_Factory;
    public AudioClip intensity_Factory;
    public AudioClip start_Forest;
    //  public AudioClip lowIntensity_Forest;
    public AudioClip intensity_Forest;
    // public AudioClip lowIntensity_Town;
    public AudioClip intensity_Town;
    public AudioClip loop_TitleScreen;
    public AudioClip start_TitleScreen;
    public AudioClip loop_Credits;
    public AudioClip start_Credits;
    public AudioSource lowSource;
    public AudioSource highSource;
  

    private bool hasStarted;
    public string soundtype;
    public static bool isInCombat;
    private bool isPlaying = false;
    private string highourceTag;
    private string lowSourceTag;

    // Use this for initialization
    void Start()
    {
        hasStarted = false;
        highourceTag = "";
        lowSourceTag = "";
    }

    // Update is called once per frame
    //set audio for music per level
    void Update()
    {

        switch (soundtype)
        {
            case "Forest":
                lowSource.mute = false;
                highSource.mute = true;
                HasStartSoundLow(hasStarted, start_Forest);
                if (hasStarted)
                {
                    SetAudioLow(soundtype, intensity_Forest);
                }
                break;
            case "Factory":
                lowSource.mute = false;
                highSource.mute = true;
                SetAudioLow(soundtype, intensity_Factory);
                break;
            case "Town":
                lowSource.mute = false;
                highSource.mute = true;
                SetAudioLow(soundtype, intensity_Town);
                break;
            case "Credits":
                lowSource.mute = false;
                highSource.mute = true;
                HasStartSoundLow(hasStarted, start_Credits);
                if (hasStarted)
                {
                    SetAudioLow(soundtype, loop_Credits);
                }
                break;
            default:
                HasStartSoundLow(hasStarted, start_TitleScreen);
                if (hasStarted)
                {
                    lowSource.mute = false;
                    highSource.mute = true;
                    SetAudioLow(soundtype, loop_TitleScreen);
                }
                break;
        }
    }


    //sets the high intencity audio source.
    private void SetAudioHigh(string audioName, AudioClip audio)
    {
        if (highourceTag != audioName)
        {
            highSource.Pause();
            highSource.clip = audio;
            highSource.Play();
            highourceTag = audioName;
            highSource.loop = true;
        }

    }
    //sets the low intencity audio
    private void SetAudioLow(string audioName, AudioClip audio)
    {
        if (lowSourceTag != audioName)
        {
            lowSource.Pause();
            lowSource.clip = audio;
            lowSource.Play();
            lowSourceTag = audioName;
            lowSource.loop = true;
        }

    }

    //if theire is a start sound play it no loop than swich to loop version after
    private bool HasStartSoundHigh(bool isStarted)
    {
        if (!isStarted)
        {
            if (!isPlaying)
            {
                highSource.PlayOneShot(start_Forest);
                highSource.loop = false;
                isPlaying = true;
                return true;
            }
            else
            {
                return true;
            }

        }
        else if (!hasStarted && highSource.isPlaying == false)
        {
            hasStarted = true;
            isPlaying = false;
            return true;
        }
        else return true;
    }
    //if theire is a start sound play it no loop than swich to loop version after
    private bool HasStartSoundLow(bool isStarted, AudioClip sound)
    {
        if (!isStarted)
        {
            if (!isPlaying)
            {
                lowSource.Pause();
                lowSource.clip = sound;
                lowSource.Play();
                lowSource.loop = false;
                isPlaying = true;
                hasStarted = true;
                return true;
            }
            else
            {
                return true;
            }

        }
        else if (!isStarted && lowSource.isPlaying == false)
        {
            hasStarted = true;
            isPlaying = false;
            return true;
        }
        else return true;
    }

    public void inCombat(bool combat)
    {
        isInCombat = combat;
    }

   

 
}
