﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostileRangedSounds : BaseSounds {

    //audio for ranged hostiles
    //set up in inspector
    
    public AudioClip rangedEnemy_Death;
    public AudioClip[] rangedEnemy_Attacks;
    public AudioClip[] enemyHit;
    public AudioClip[] rangedEnemy_Walks;


    // Use this for initialization
   

    public void RangedAttackAudio()
    {
        AudioClip audio = rangedEnemy_Attacks[Random.Range(0, rangedEnemy_Attacks.Length)];
        SetAudio(audio);
    }
    public void HitAudio()
    {
        AudioClip audio = enemyHit[Random.Range(0, enemyHit.Length)];
        SetAudio(audio);
    }
    public void RangedWalksAudio()
    {
        AudioClip audio = rangedEnemy_Walks[Random.Range(0, rangedEnemy_Walks.Length)];
        SetAudio(audio);
    }

    public void RangedEnemy_DeathAudio()
    {
        source.Stop();
        source.PlayOneShot(rangedEnemy_Death);
    }

    
}
