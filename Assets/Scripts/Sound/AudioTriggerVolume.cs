﻿using UnityEngine;
using System.Collections;

public class AudioTriggerVolume : MonoBehaviour {
    //This class is used to change the music which the player encounters.
    //It is likely better to have a controller class for all Audio TriggerVolumes based on enemies.
    //We only want the volume to trigger once, therefore a bool is required.
    private bool isTriggered = false;
    public string soundTypes = "";
     private new AudioSource audio;

    //This is used for initialization
    void Start()
    { 
        //The audio to be played is attached to the 
        audio = GetComponent<AudioSource>();
    }
        //When the player enters the trigger area we switch to the audio determined in the inspector.
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "AudioSource")
        {
            //We set the triggerVolume to true to keep it from being triggered.
            isTriggered = true;
            other.GetComponent<AmbientSounds>().soundtype = soundTypes;
        }
        if(other.tag == "Player" && other.GetComponent<PlayerSound>() != null)
        {
            other.GetComponent<PlayerSound>().soundtype = soundTypes;
        }
    }
}
