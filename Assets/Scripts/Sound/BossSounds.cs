﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSounds : BaseSounds {

    //sound for boss
    public AudioClip ambient;

    public AudioClip[] hit;

    public AudioClip[] groan;

	// Use this for initialization
	void Start () {
        source = gameObject.GetComponent<AudioSource>();
        source.clip = ambient;
	}

    public void HitAudio()
    {
        AudioClip audio = hit[Random.Range(0, hit.Length)];
        SetAudio(audio);
    }

    public void GroanAudio()
    {
        AudioClip audio = groan[Random.Range(0, groan.Length)];
        SetAudio(audio);
    }

}
