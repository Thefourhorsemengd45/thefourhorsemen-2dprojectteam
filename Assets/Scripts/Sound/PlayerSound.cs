﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSound : BaseSounds {

    //arrays of audio clip types
    public AudioClip[] footsteps_Factory;
    public AudioClip[] footsteps_Forest;
    public AudioClip[] footsteps_Town;
    public AudioClip[] melee_Attack;
    public AudioClip[] ranged_Attack;
    public AudioClip[] allLeversPulled;
    public AudioClip[] locked_Door;
    public AudioClip[] pulled_Lever;
    public AudioClip[] dying;
    public AudioClip[] hit;
    public AudioClip[] meleeAttack_Voice;
    public AudioClip[] pulling_Lever;

    public AudioClip[] rangedAttack_Voice;
    public AudioClip[] suprised;
    public AudioClip[] tired;

    public string soundtype = "Forest";

    // Use this for initialization
    void Start ()
    {
        source = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    //changes the footsteps basied on location
    public void FootstepsAudio()
    {
        AudioClip audio;
        switch (soundtype)
        {
          
            case "Forest":
              audio = footsteps_Forest[Random.Range(0, footsteps_Forest.Length)];
                break;
            case "Factory":
                audio = footsteps_Factory[Random.Range(0, footsteps_Factory.Length)];
                break;
            case "Town":
                audio = footsteps_Town[Random.Range(0, footsteps_Town.Length)];
                break;
            default:
                audio = footsteps_Forest[Random.Range(0, footsteps_Forest.Length)];
                break;
        }

         
        SetAudio(audio);
    }

    public void MeleeAttackAudio()
    {
        AudioClip audio = melee_Attack[Random.Range(0, melee_Attack.Length)];
        SetAudio(audio);
    }
    public void RangedAttackAudio()
    {
        AudioClip audio = ranged_Attack[Random.Range(0, ranged_Attack.Length)];
        SetAudio(audio);
    }
    public void AllLeversPulledAudio()
    {
        AudioClip audio = allLeversPulled[Random.Range(0, allLeversPulled.Length)];
        SetAudio(audio);
    }
    public void LockedDoorAudio()
    {
        AudioClip audio = locked_Door[Random.Range(0, locked_Door.Length)];
        SetAudio(audio);
    }
    public void PulledLeverAudio()
    {
        AudioClip audio = pulled_Lever[Random.Range(0, pulled_Lever.Length)];
        SetAudio(audio);
    }
    public void dyingAudio()
    {
        AudioClip audio = dying[Random.Range(0, pulled_Lever.Length)];
        source.Stop();
        SetAudio(audio);
    }
    public void HitAudio()
    {
        AudioClip audio = hit[Random.Range(0, hit.Length)];
        SetAudio(audio);
    }
    public void MeleeAttackVoice()
    {
        AudioClip audio = meleeAttack_Voice[Random.Range(0, meleeAttack_Voice.Length)];
        SetAudio(audio);
    }
    public void PullingLeverAudio()
    {
        AudioClip audio = pulling_Lever[Random.Range(0, pulling_Lever.Length)];
        SetAudio(audio);
    }
    public void RangedAttackVoice()
    {
        AudioClip audio = rangedAttack_Voice[Random.Range(0, rangedAttack_Voice.Length)];
        SetAudio(audio);
    }
    public void SuprisedAudio()
    {
        AudioClip audio = suprised[Random.Range(0, suprised.Length)];
        SetAudio(audio);
    }
    public void TiredAudio()
    {
        AudioClip audio = tired[Random.Range(0, tired.Length)];
        source.clip = audio;
        source.Play();
    }

    public void Stop()
    {
        source.Stop();
    }

    public void StopClip()
    {
        source.clip = null;
    }


}
