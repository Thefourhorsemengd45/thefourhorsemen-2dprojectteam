﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerSetter : MonoBehaviour
{

    private float layerMuiltiplyer = -10;
    private SpriteRenderer objectRenderer;
    private Transform objectTransform;
    public int layerModifyer = 0;
    public float offset = 0;

    // Use this for initialization
    void Start()
    {
       

            objectRenderer = GetComponent<SpriteRenderer>();
        
      
        objectTransform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
            objectRenderer.sortingOrder = (int)((objectTransform.position.y + offset) * layerMuiltiplyer) + layerModifyer;
        
            GetComponent<SpriteRenderer>().sortingOrder = objectRenderer.sortingOrder;
      

    }
}
