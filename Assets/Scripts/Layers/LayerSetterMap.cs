﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerSetterMap : MonoBehaviour {


    private float layerMuiltiplyer = -10;
    private Renderer objectRenderer;
    private Transform objectTransform;
    public string sortname;
    public GameObject[] meshes;

    //Sets the sorting layer to the map layer properly positioned
    // Use this for initialization
    void Start()
    {
        foreach (GameObject mesh in meshes)
        {
            objectRenderer = mesh.GetComponent<Renderer>();
            objectRenderer.sortingLayerName = sortname;
            mesh.GetComponent<Renderer>().sortingLayerName = objectRenderer.sortingLayerName;
        }
    }


}
