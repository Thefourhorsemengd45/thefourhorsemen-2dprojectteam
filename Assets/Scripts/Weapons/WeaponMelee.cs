﻿using UnityEngine;
using System.Collections;

public class WeaponMelee : WeaponBase
{
    //Unit Melee Range
    public float hitDistance = 1.5f;

    // Update is called once per frame
    void Update()
    {
        //sets anim to units animator
        anim = GetComponent<Animator>();
        //counts down cooldown and gets target(located in WeaponBase)
        Cooldown();
        GetTarget();
    }

    //Unit makes the melee attack in the direction they are faceing
    public void MeleeAttack(float direction)
    {   //attack timer set to the cooldown
        attackTimer = cooldown;
        //distance between target and attacking unit
        float distance = Vector2.Distance(target.transform.position, transform.position);

        //is the target within the player attack range
        if (distance < hitDistance)
        {
            //attack direction is where the target is
            if (direction > 0)
            {
                //Debug.Log("hit");
                //get the unit being attacked
                BaseUnit unit = target.GetComponent<BaseUnit>();
                //damage their health by damage value(located in BaseUnit)
                //knock back to hit unit in direction oposite the Unit hiting them came from (BaseUnit)
                StartCoroutine(unit.TakenDamage(anim.GetFloat("HorizontalSpeed"), anim.GetFloat("VerticalSpeed"), damageIntencity, -damage));
            }
        }

    }

 
}
