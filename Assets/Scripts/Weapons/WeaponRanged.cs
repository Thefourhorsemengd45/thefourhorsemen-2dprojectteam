﻿using UnityEngine;
using System.Collections;

public class WeaponRanged : WeaponBase
{
    //Velocity of the projectile
    public float rangedVelocity = 5;
    //actual projectile
    public Projectile projectilePrefab;


    // Update is called once per frame
    void Update()
    {
        //Geting unit position
        anim = GetComponent<Animator>();
        //Manages Cooldown of shot (Located in WeaponBase)
        Cooldown();
        //get target shot will be directed towards (Located in WeaponBase)
        GetTarget();
    }

    //fires projectile
    public void FireProjectile()
    {
        attackTimer = cooldown;
        //the target is in existance fire towards the player
        Vector3 dir = target.transform.position - transform.position;
        dir.Normalize();

        //calculate the zAngle to rotate towards the target
        float zAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90;
        //rotation towards the target
        Quaternion direction = Quaternion.Euler(0, 0, zAngle);
        //instanciate the projectile
        Projectile projectile = (Projectile)Instantiate(projectilePrefab, transform.position, direction);
        //set the projetile to the shooters layer
        projectile.gameObject.layer = this.gameObject.layer;
    }
}
