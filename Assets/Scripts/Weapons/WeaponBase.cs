﻿using UnityEngine;
using System.Collections;

public class WeaponBase : MonoBehaviour
{
    //Unit target
    protected GameObject target;
    //Unit rigidbody
    protected Rigidbody2D rb;
    //Timer before next attack alowed
    [HideInInspector]
    public float attackTimer = 0;
    //seting for the timer
    public float cooldown = 2;
    //damage weapon inflics
    public float damage = 1;
    //Unit animator
    protected Animator anim;
    //Power each hit contans for knockback
    public float damageIntencity = 3;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Animator anim = GetComponent<Animator>();
    }

    //cooldown countdown for weapons
    protected void Cooldown()
    {
        if (attackTimer > 0)
        {
            attackTimer -= Time.deltaTime;
        }
    }

    //target of the weapon for the player else target the player
    protected void GetTarget()
    {
        if (GetComponent<Targeting>() != null)
        {
            target = GetComponent<Targeting>().target;
        }
        else target = GetComponent<BaseHostile>().player.gameObject;
    }

    //direction  the target is in relative to the Unit
    //if direction is greater than 0 target is infront of Unit 
    public float HostileDirection()
    {
        float direction = 0;
        Vector2 dir = (target.transform.position - transform.position).normalized;
        direction = Vector2.Dot(dir, new Vector2(anim.GetFloat("HorizontalSpeed"), anim.GetFloat("VerticalSpeed")));


        return direction;
    }
}
