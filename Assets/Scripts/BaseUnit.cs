﻿using UnityEngine;
using System.Collections;

public class BaseUnit : MonoBehaviour
{
    //unit rigidbady
    protected Rigidbody2D rb;
    //unit sprite renderer
    protected SpriteRenderer sr;
    //unit animator
    protected Animator anim;
    //unit weapon melee
    protected WeaponMelee melee;
    //unit weapon ranged
    protected WeaponRanged ranged;
    //unit movement speed
    public float speed;
    //unit health
    public float health = 1;
    //unit health since last damage
    protected float tempHealth;
    private Vector2 knockBack;
    //time it takes for animation to play
    public float animationTime = 2;
    private float playerParameter = 5;
    protected bool coroutineIsRunning = false;
    protected bool attacking = false;
    private int flashAmmount = 3;
    private bool wasHit = false;
    private float timePerColourSwap = .1f;

    // Use this for initialization
    void Start()
    {
        //assign rb variable once it points towards the Riged body owner
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        //assings unit weapons and checks to see if they have them
        if (GetComponent<WeaponMelee>() != null)
        {
            melee = GetComponent<WeaponMelee>();
        }
        if (GetComponent<WeaponRanged>() != null)
        {
            ranged = GetComponent<WeaponRanged>();
        }

        tempHealth = health;
    }

    //movement of any unit and sets their animation
    protected void Move(float horizontalInput, float verticalInput)
    {
        Vector2 vel = rb.velocity;
        vel.x = horizontalInput * speed;
        vel.y = verticalInput * speed;
        
        if (knockBack.magnitude <= 1)
        {
            rb.velocity = vel;
            if (!attacking)
            {
                knockBack = Vector2.zero;
                //set the velocity to a vertor and then applys the velocity and then apply the actual velocity to the rb
                anim.SetFloat("VerticalSpeed", verticalInput);
                anim.SetFloat("HorizontalSpeed", horizontalInput);
            }
        }
        else
        {
            rb.velocity = knockBack;
            knockBack *= 0.9f; 
        }
         
    }


    //stops unit in place 
    protected void StopInPlace()
    {
        Vector2 vel = Vector2.zero;
        
    }

    //if unit takes damage calculates knockback from the direction the unit
    //was hit from multiplyed by the force of the hit
    internal IEnumerator TakenDamage(float x, float y, float intesity, float healthAdjustment)
    {
        yield return StartCoroutine(WasHit(healthAdjustment, x, y, intesity));
        yield return null;
    }

    //when the unit has finish their attack set animation
    //back to movement.
    //hostiles only
    protected void AttackEnded()
    {
        anim.SetBool("Attack", false);
        if (melee != null)
        { melee.attackTimer = melee.cooldown; }
        if(ranged != null)
        { ranged.attackTimer = ranged.cooldown; }
        if(anim.parameterCount >= playerParameter)
        {
            anim.SetBool("RangedAttack", false);
        }
    }

    ///added as patch to fix player attack cooldowns ****
    protected void AttackEndedMelee()
    {
        anim.SetBool("Attack", false);
        if (melee != null)
        { melee.attackTimer = melee.cooldown; }

    }
    protected void AttackEndedRanged()
    {
        anim.SetBool("Attack", false);       
        if (ranged != null)
        { ranged.attackTimer = ranged.cooldown; }
        if (anim.parameterCount >= playerParameter)
        {
            anim.SetBool("RangedAttack", false);
        }
    }

    /// *******
 
    //unit goes into idle mode when target out of range
    protected void Idle()
    {
        anim.SetFloat("VerticalSpeed", 0);
        anim.SetFloat("HorizontalSpeed", 0);
        rb.velocity = Vector2.zero;
    }

    //damages the unit when hit
    IEnumerator WasHit(float healthdamage, float x, float y, float intesity)
    {
        if (wasHit == false)
        {
            health += healthdamage;
            Vector2 knockBackintensity;
            if (intesity != 0)
            {
                knockBackintensity = new Vector2(x, y) * intesity;
            }
            else
            {
                knockBackintensity = Vector2.zero;
            }
            yield return StartCoroutine(KnockBack(knockBackintensity));
            yield return null;
        }
        else
        { yield return null; }

    }

    //calls the knockback for unit when hit
    IEnumerator KnockBack(Vector2 knockBackpower)
    {
        knockBack = knockBackpower;
        yield return StartCoroutine(Painsound());
        yield break;
    }

    //when the unit gets hit plays their particualr get hit sound
    IEnumerator Painsound()
    {
        if (gameObject.tag == "Player")
        { GetComponent<PlayerSound>().HitAudio(); }
        else if (GetComponent<HostileMelleSounds>() != null)
        { GetComponent<HostileMelleSounds>().HitAudio(); }
        else if ((GetComponent<HostileRangedSounds>() != null))
        { GetComponent<HostileRangedSounds>().HitAudio(); }
        yield return ColourSwap(0);
        yield return null;
    }

    //when hit units flash
    IEnumerator ColourSwap(int i)
    {
        if (i < flashAmmount && gameObject.GetComponent<SpriteRenderer>())
        {
            wasHit = true;
            if (gameObject.GetComponent<SpriteRenderer>().color == Color.white)
            {
                gameObject.GetComponent<SpriteRenderer>().color = Color.red;
            }
            else
            {
                gameObject.GetComponent<SpriteRenderer>().color = Color.white;
                i++;
            }
            yield return new WaitForSeconds(timePerColourSwap);
            yield return StartCoroutine(ColourSwap(i));
        }
        else
        {
            wasHit = false;
            yield break;
        }
    }
}
