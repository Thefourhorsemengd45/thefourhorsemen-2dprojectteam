﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    //speed of prjectile
    public float projectilevelocity = 5f;
    //damage projectile inflicts on hit
    public int healthModifyer = 1;
    //Time before the gameobject destroys itself
    private float destroyTime = 5;
    //knockback
    public float damageIntencity = 10;


    // Use this for initialization
    void Start()
    {
        //lanches the prjectile in direction of target
        GetComponent<Rigidbody2D>().velocity = transform.up * projectilevelocity;
        //Destroy this gameobject after 5 seconds.
        Destroy(gameObject, destroyTime);
    }

    // Update is called once per frame
    void Update()
    {
    }

    //if collides with unit damage them
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Hostile" || collider.tag == "Player")
        {
            if (collider.GetComponent<Projectile>() == null)
            {
                if (collider.tag != tag)
                {
                    BaseUnit unit = collider.GetComponent<BaseUnit>();
                    StartCoroutine(unit.TakenDamage(GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y, damageIntencity, -healthModifyer));

                }
            }
        }
    }
}
