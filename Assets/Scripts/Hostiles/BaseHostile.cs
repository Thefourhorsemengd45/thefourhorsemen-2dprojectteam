﻿using UnityEngine;
using System.Collections;

public class BaseHostile : BaseUnit
{
    //sets the target for the Hostile AI
    public Transform player;
    //distance before AI attacks player
    public float distanceFromPlayerBeforeAttack = 2;
    //distance before AI will notice the player
    public float engagementRange = 20;

    //checks if the player is within range of particular distance and returs true or false.
    protected bool IsWithinRange(float distance)
    {
        //calculates player and unit distance apart
        if (Vector2.Distance(player.position, transform.position) <= distance)
        { 
            return true;
        }
        else
            return false;
    }

    //tells the Unit to move towards to attack the player
    protected void MoveTowardsPlayer()
    {
        Vector3 dir = (player.transform.position - transform.position).normalized;

        //unit moves (located in BaseUnit)
        Move(dir.x, dir.y);
    }

    // tells the Unit to move away from the player
    protected void MoveAwayFromPlayer()
    {
        Vector3 dir = -(player.transform.position - transform.position).normalized;

        //unit moves (located in BaseUnit)
        Move(dir.x, dir.y);
    }

    public void deleteColider()
    {
        Collider2D collid = gameObject.GetComponent<Collider2D>();

        collid.isTrigger = true;
    }

}
