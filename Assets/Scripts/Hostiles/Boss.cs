﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Boss : BaseHostile {

    public float timeDefence;
    public float timeFlame;
    public float timeDeath;
    public float timeTakenDamage;
    public  Animator animtion;
    private int setScene = 6;
    private bool isAngry = false;
    private bool isDefending = false;
    private bool takenDamage = false;
    private bool isAlive = true;
    private GameObject[] hostiles;
    private float bosshealth = 1;
    public AudioClip bossdeath;
    private AudioSource[] allAudioSources;
    //acounts for particular unit health bug that prevents boss from being killed
    private float bosshealthedit = -5;
    private bool isEnraged = false;

    private void Start()
    {
        anim = animtion;
    }
    // Update is called once per frame
    void Update () {
        
        hostiles = GameObject.FindGameObjectsWithTag("Hostile");
        if (bosshealth > 0)
        {
            bosshealth = bosshealthedit;
            foreach (GameObject hostile in hostiles)
            {
                if (hostile.GetComponent<BaseUnit>() != null)
                {
                    bosshealth += hostile.GetComponent<BaseUnit>().health;
                }
            }
            if (isEnraged == false)
            {
                StartCoroutine(Anger());
            }
        }
        else
        {
            if (isAlive == true)
            {
                anim.SetBool("isDefending", false);
                if (health <= 0)
                {
                    StartCoroutine(TakenDamage());
                }
            }
        }
        
    }
    //boss keeps shild up
    IEnumerator Defending()
    {
        gameObject.GetComponent<Boss>().health = 1;
        isDefending = true;
        anim.SetBool("isDefending", isDefending);
         if(bosshealth <= 1)
        {
            isDefending = false;
            anim.SetBool("isDefending", isDefending);
        }
        yield return null;
    }
    //when the boss gets hit
    IEnumerator TakenDamage()
    {
        if (takenDamage == false)
        {
            takenDamage = true;
            isAlive = false;
            anim.SetBool("TakenDamage", takenDamage);
            yield return new WaitForSeconds(timeTakenDamage);
            takenDamage = false;
            anim.SetBool("TakenDamage", takenDamage);
            takenDamage = true;
            StartCoroutine(Dead());
        }
        yield return null;
    }
    //when boss is enraged
    IEnumerator Anger()
    {
        if (isAngry == false)
        {
            isEnraged = true;
            isAngry = true;
            anim.SetBool("isAngry", isAngry);
          yield return new WaitForSeconds(timeFlame);
            anim.SetBool("isAngry", false);
            isEnraged = false;
            yield return null;
        }
        else
        {
            StartCoroutine(Defending());
        }
    }
    //when boss dies
    IEnumerator Dead()
    {
        if (bosshealth <= 0)
        {
            anim.SetBool("TakenDamage", false);
            isAngry = false;
            StartCoroutine(Anger());           
            anim.SetBool("isAlive", isAlive);
            bosskilled();
            yield return new WaitForSeconds(timeDeath);
            SceneSetter.SetScene(setScene);
            SceneManager.LoadScene(1);
            yield break;
        }
        else
            yield break;
    }
    //Stop all sounds
    public void bosskilled()
    {
        StopAllAudio();
        gameObject.GetComponentInParent<AudioSource>().PlayOneShot(bossdeath);

    }

    void StopAllAudio()
    {
        allAudioSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        foreach (AudioSource audioS in allAudioSources)
        {
            audioS.Stop();
        }
    }
}
