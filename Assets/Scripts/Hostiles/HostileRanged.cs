﻿using UnityEngine;
using System.Collections;

public class HostileRanged : BaseHostile
{
    //if the melee animation should play or not
    private bool isRangedAnim = false;
    //prevents multiple hits from melee
    private bool canRangedAttack = true;
    public float retreatRange = 10;


    void Update()
    {
        //if player is within engagment range start doing actions
        if (IsWithinRange(engagementRange))
        {
            //if the unit is alive do actions
            if (health > 0)
            {
                //if player not in range
                if (isRangedAnim == false)
                {
                    attacking = false;
                    //move towards player(Located in BaseHostile)
                    //else retreat
                    if (IsWithinRange(retreatRange) == false)
                    {
                        //check if in range(Located in BaseHostile)
                        isRangedAnim = IsWithinRange(distanceFromPlayerBeforeAttack);
                        MoveTowardsPlayer();
                        anim.SetBool("Attack", isRangedAnim);
                    }
                    else
                    {
                        MoveAwayFromPlayer();
                    }
                    //set the attack animation true or false depends on if in range
                   

                    ranged.attackTimer = ranged.cooldown;
                }
                //if in attack range
                else
                {
                    Move(0,0);
                }
            }
            //if unit is dead stop all actions and death animation
            else
            {   //located it BaseUnit
                Idle();
                isRangedAnim = false;
                anim.SetBool("Death", true);
                anim.SetBool("Attack", isRangedAnim);
            }

        }
        //else dont move
        else
        {
            //located it BaseUnit
            Idle();
        }

    }

    public void DoRangedHit()
    {
        if (!coroutineIsRunning)
            StartCoroutine(RanedAttack());
    }
    IEnumerator RanedAttack()
    {
        coroutineIsRunning = true;
        isRangedAnim = true;

        yield return StartCoroutine(DoRangedAttack());

        yield return null;
    }
    IEnumerator DoRangedAttack()
    {
        ranged.FireProjectile();
        yield return new WaitForSeconds(animationTime);
        isRangedAnim = false;
        coroutineIsRunning = false;
        yield break;
    }
}
