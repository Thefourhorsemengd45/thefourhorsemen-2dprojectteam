﻿using UnityEngine;
using System.Collections;

public class HostileMelee : BaseHostile
{
    //if the melee animation should play or not
    private bool isMeleeAnim = false;
    //prevents multiple hits from melee
    private bool canMeleeAttack = true;

    void Update()
    {
        //if player is within engagment range start doing actions
        if (IsWithinRange(engagementRange))
        {
            //if the unit is alive do actions
            if (health > 0)
            {
                //if player not in melee range
                if (isMeleeAnim == false)
                {
                    attacking = false;
                    //check if in melee range(Located in BaseHostile)
                    isMeleeAnim = IsWithinRange(distanceFromPlayerBeforeAttack);
                    //move towards player(Located in BaseHostile)
                    MoveTowardsPlayer();
                    //set the attack animation true or false depends on if in range
                    anim.SetBool("Attack", isMeleeAnim);

                }
                else
                {
                    attacking = true;
                    Move(0, 0);
                }

            }
            //if unit is dead stop all actions and death animation
            else
            {   //located it BaseUnit
                Idle();
                isMeleeAnim = false;
                anim.SetBool("Death", true);
                anim.SetBool("Attack", isMeleeAnim);
                health = 0;
            }

        }
        //else dont move
        else
        {
            Idle();           
        }

    }


    IEnumerator MeleeAttack()
    {
        coroutineIsRunning = true;
        isMeleeAnim = true;
            //Is the player within attack direction (located in WeaponMelee)
            float dir = melee.HostileDirection();
            //stops unit in place to play attach anim() (Located in BaseUnit)

        yield return StartCoroutine(DoMelee(dir));
  
        yield return null;
    }

    IEnumerator DoMelee(float dir)
    {
        melee.MeleeAttack(dir);
        yield return new WaitForSeconds(animationTime);
        isMeleeAnim = false;
        coroutineIsRunning = false;
        yield break;
    }

    public void DoMeleeAttack()
    {
        if(!coroutineIsRunning)
        StartCoroutine(MeleeAttack());
    }

}
