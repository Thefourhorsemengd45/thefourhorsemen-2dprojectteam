﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateArea : MonoBehaviour {

    
    public BaseUnit[] hostilesArray;
    protected Animator anim;                                  
    private float gateHealth;
    private bool hasStarted = false;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!hasStarted)
        {
            //check slected hostiles if they are alive
            gateHealth = 0;
            foreach (BaseUnit hostile in hostilesArray)
            {
                gateHealth += hostile.health;
            }

            //if all hostiles dead open gate
            if (gateHealth == 0)
            {

                StartCoroutine(openGate());
            }
        }

       }

    IEnumerator openGate()
    {
        hasStarted = true;
        anim.SetBool("OpenGate", hasStarted);
        yield break;
    }

    public void deleteColider()
    {
      Collider2D collid = gameObject.GetComponent<Collider2D>();

        collid.isTrigger = true;
    }
    }
