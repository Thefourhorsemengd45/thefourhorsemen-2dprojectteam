﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    //spawner class for select hostile type
     public GameObject rangedHostile;
    public GameObject meleeHostile;

    public string spawnType = "Melee";

    private GameObject[] hostileArray;

    //tracks spawned hostle
    private BaseHostile hostile;

    public Transform player;
    //amount of units that the spawner will spawn before stoping
    public float spawnhealth = 5;
    // Use this for initialization
    void Start () {
        //always spawn melee at start of level
        hostile = Instantiate(meleeHostile, transform.position, transform.rotation).GetComponent<HostileMelee>();
        hostile.player = player;
    }
	
	// Update is called once per frame
	void Update () {
        //when out of spawns destroys itself
        if(spawnhealth <= 0)
        {
            Destroy(gameObject);
        }
        
       
        //only spawn if previsous hostile dead
        if(hostile.health <= 0)
        {
            //spawn particular hostile type
            if (spawnType == "Ranged")
            {
                StartCoroutine(SpawnRanged());
            }
            else
            {
                StartCoroutine(SpawnMelee());
            }
        }

		
	}

    IEnumerator SpawnRanged()
    {
        hostile = Instantiate(rangedHostile, transform.position, transform.rotation).GetComponent<HostileRanged>();

        hostile.player = player;
        spawnhealth -= 1;
        yield return null;
    }
    IEnumerator SpawnMelee()
    {
       hostile = Instantiate(meleeHostile, transform.position, transform.rotation).GetComponent<HostileMelee>();
        hostile.player = player;
        spawnhealth -= 1;
        yield return null;
    }
}

