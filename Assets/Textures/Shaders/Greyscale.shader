﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Greyscale" {


Properties{
	_MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
_EffectAmount("Effect Amount", Range(0, 1)) = 1.0
_ColourRadius1("ColourRadius1", Float) = .00001
_ColourRadius2("ColourRadius2", Float) = .00001
_ColourRadius3("ColourRadius3", Float) = .00001
_ColourRadius4("ColourRadius4", Float) = .00001
_ColourRadius5("ColourRadius5", Float) = .00001
_ColourMaxRadius("ColourMaxRadius", Float) = 0.5
_PColourRadius("PlayerColourRadius", Float) = 1.0
_PColourMaxRadius("PlayerColourMaxRadius", Float) = 1.0
_ColourR("ColourR", Float) = 0.3
_ColourG("ColourG", Float) = 0.59
_ColourB("ColourB", Float) = 0.11
_Colour_Death_Zone1("_Colour_Death_Zone1", Vector) = (0,0,0,1)
_Colour_Death_Zone2("_Colour_Death_Zone2", Vector) = (0,0,0,1)
_Colour_Death_Zone3("_Colour_Death_Zone3", Vector) = (0,0,0,1)
_Colour_Death_Zone4("_Colour_Death_Zone4", Vector) = (0,0,0,1)
_Colour_Death_Zone5("_Colour_Death_Zone5", Vector) = (0,0,0,1)

_PlayerPos("_PlayerPos", Vector) = (0,0,0,1)

}
SubShader{
	Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
	LOD 200
	Blend SrcAlpha OneMinusSrcAlpha
	cull off

	CGPROGRAM
#pragma surface surf Lambert vertex:vert alpha:blend 
#include "Tessellation.cginc"	
	sampler2D _MainTex;
uniform float _EffectAmount;
float _ColourRadius1;
float _ColourMaxRadius;
float _ColourRadius2;
float _ColourRadius3;
float _ColourRadius4;
float _ColourRadius5;
float _PColourRadius;
float _PColourMaxRadius;
float4 _Colour_Death_Zone1;
float4 _Colour_Death_Zone2;
float4 _Colour_Death_Zone3;
float4 _Colour_Death_Zone4;
float4 _Colour_Death_Zone5;
float4 _PlayerPos;
float _ColourR;
float _ColourG;
float _ColourB;

struct Input
{
	float2 uv_MainTex;
	float2 location;
};

struct appdata {
	float4 vertex : POSITION;
	float4 tangent : TANGENT;
	float3 normal : NORMAL;
	float2 texcoord : TEXCOORD0;
};


float powerForPos1(float4 pos, float2 nearVertex);
float powerForPos2(float4 pos, float2 nearVertex);
float powerForPos3(float4 pos, float2 nearVertex);
float powerForPos4(float4 pos, float2 nearVertex);
float powerForPos5(float4 pos, float2 nearVertex);
float playerPos(float4 pos, float2 nearVertex);


void vert(inout appdata_full vertexData, out Input outData)
{
	float4 pos = UnityObjectToClipPos(vertexData.vertex);
	float4 posWorld = mul(unity_ObjectToWorld, vertexData.vertex);
	outData.uv_MainTex = vertexData.texcoord;
	outData.location = posWorld.xy;
}



void surf(Input IN, inout SurfaceOutput o)
{
	fixed4 baseColour = tex2D(_MainTex, IN.uv_MainTex);


	float alpha = (1.0 - (playerPos(_PlayerPos, IN.location) + powerForPos1(_Colour_Death_Zone1, IN.location) + powerForPos2(_Colour_Death_Zone2, IN.location) + powerForPos3(_Colour_Death_Zone3, IN.location) + powerForPos4(_Colour_Death_Zone4, IN.location) + powerForPos5(_Colour_Death_Zone5, IN.location)));

	o.Albedo = lerp(baseColour.rgb, dot(baseColour.rgb, float3(_ColourR, _ColourG, _ColourB)), _EffectAmount);
	o.Alpha = alpha * baseColour.a;
}

float powerForPos1(float4 pos, float2 nearVertex)
{
	float atten = clamp(_ColourRadius1 - length(pos.xy - nearVertex.xy), 0.0, _ColourRadius1);
	return (1.0 / _ColourMaxRadius)*atten / _ColourRadius1;
}
float powerForPos2(float4 pos, float2 nearVertex)
{
	float atten = clamp(_ColourRadius2 - length(pos.xy - nearVertex.xy), 0.0, _ColourRadius2);
	return (1.0 / _ColourMaxRadius)*atten / _ColourRadius2;
}
float powerForPos3(float4 pos, float2 nearVertex)
{
	float atten = clamp(_ColourRadius3 - length(pos.xy - nearVertex.xy), 0.0, _ColourRadius3);
	return (1.0 / _ColourMaxRadius)*atten / _ColourRadius3;
}
float powerForPos4(float4 pos, float2 nearVertex)
{
	float atten = clamp(_ColourRadius4 - length(pos.xy - nearVertex.xy), 0.0, _ColourRadius4);
	return (1.0 / _ColourMaxRadius)*atten / _ColourRadius4;
}
float powerForPos5(float4 pos, float2 nearVertex)
{
	float atten = clamp(_ColourRadius5 - length(pos.xy - nearVertex.xy), 0.0, _ColourRadius5);
	return (1.0 / _ColourMaxRadius)*atten / _ColourRadius5;
}

float playerPos(float4 pos, float2 nearVertex)
{
	float atten = clamp(_PColourRadius - length(pos.xy - nearVertex.xy), 0.0, _PColourRadius);
	return (1.0 / _PColourMaxRadius)*atten / _PColourRadius;
}


ENDCG
}
Fallback "Transparent/VertexLit"



}